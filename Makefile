up:
	cd laradock && docker-compose up -d workspace rabbitmq

down:
	cd laradock && docker-compose down

jumpin:
	cd laradock && docker-compose run workspace bash