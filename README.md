#Queue Piper

Simple CLI tool to pipe data to a RabbitMQ

### Installation

    git clone git@bitbucket.org:contorion/queue-piper
    cd queue-piper
    composer install --no-dev

### Usage

    ./piper [options] [--] <queue-name>
    
    Arguments:
      queue-name               The queue to write to
    
    Options:
      --host=HOST          Host of the queue server [default: "localhost"]
      --port=PORT          Port of the queue server [default: 5672]
      --vhost=VHOST        Virtual Host of the queue server
      --user=USER          Username [default: "guest"]
      --password=PASSWORD  Password [default: "guest"]


### Development

Development can be done using laradock (http://github.com/laradock/laradock)

Setup:

    git clone git@bitbucket.org:contorion/queue-piper
    cd queue-piper
    git clone git@github.com:laradock/laradock
    cp laradock.env laradock/.env
    
To start the necessary docker containers, simply run

    make up

Enter the workspace using

    make jumpin
    
Test the script

    make jumpin
    root@43bbaa914039:/var/www# cat * | ./piper --host rabbitmq test-queue 
    
To shutdown the containers, run

    make down
    