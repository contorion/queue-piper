<?php

namespace Contorion\QueuePiper;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class PipeCommand extends Command
{
    const OPTION_PASSWORD = 'password';
    const OPTION_USERNAME = 'user';
    const OPTION_PORT = 'port';
    const OPTION_HOST = 'host';
    const OPTION_VHOST = 'vhost';
    const ARGUMENT_QUEUE_NAME = 'queue-name';

    protected function configure()
    {
        $this->setName('pipe')
            ->setDescription('Pipe STDIN to a queue');

        $this->addOption(self::OPTION_HOST, '', InputOption::VALUE_REQUIRED, 'Host of the queue server', 'localhost');
        $this->addOption(self::OPTION_PORT, '', InputOption::VALUE_REQUIRED, 'Port of the queue server', 5672);
        $this->addOption(self::OPTION_VHOST, '', InputOption::VALUE_REQUIRED, 'Virtual Host of the queue server', '/');
        $this->addOption(self::OPTION_USERNAME, '', InputOption::VALUE_REQUIRED, 'Username', 'guest');
        $this->addOption(self::OPTION_PASSWORD, '', InputOption::VALUE_REQUIRED, 'Password', 'guest');
        $this->addArgument(self::ARGUMENT_QUEUE_NAME, InputArgument::REQUIRED, 'The queue to write to');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $host = $input->getOption(self::OPTION_HOST);
        $port = $input->getOption(self::OPTION_PORT);
        $user = $input->getOption(self::OPTION_USERNAME);
        $password = $input->getOption(self::OPTION_PASSWORD);
        $vhost = $input->getOption(self::OPTION_VHOST);
        $queue = $input->getArgument(self::ARGUMENT_QUEUE_NAME);

        $connection = new AMQPStreamConnection($host, $port, $user, $password, $vhost);
        $channel = $connection->channel();

        $channel->queue_declare($queue, false, true, false, false);

        while ($content = fgets(STDIN)) {
            $message = new AMQPMessage($content);
            $channel->basic_publish($message, '', $queue);
        }
    }

}